/*! *****************************************************************************
Copyright (c) NReum Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

/////////////////////////////
/// Window APIs
/////////////////////////////

declare namespace OAuth {
    interface RequestParams {
      url: string;
      method: 'GET' | 'POST' | 'PUT' | 'DELETE';
      headers?: Record<string, string>;
      params?: Record<string, string>;
      body?: Record<string, any>;
    }
  
    interface OAuthConfig {
      baseUrl: string;
      consumerKey: string;
      consumerSecret: string;
      accessToken: string;
      accessTokenSecret: string;
    }
  
    function get(requestParams: RequestParams, oauthConfig: OAuthConfig): Promise<any>;
    function post(requestParams: RequestParams, oauthConfig: OAuthConfig): Promise<any>;
    function put(requestParams: RequestParams, oauthConfig: OAuthConfig): Promise<any>;
    function del(requestParams: RequestParams, oauthConfig: OAuthConfig): Promise<any>;
  }
  
/**
 * Creates a new OAuth.
 */
interface OAuth extends OAuthEventTarget {
    /**
     * This function return this interface
     */
    isEmpty(): OAuthConstructor;

    /**
     * Calls the OAuth, substituting the specified object for the this value of the OAuth,
     * and the specified array for the arguments of the OAuth.
     * @param thisArg The object to be used as the this object.
     * @param argArray A set of arguments to be passed to the OAuth.
     */
    apply(this: OAuth, thisArg: any, argArray?: any) : any;

    /**
     * Calls a method of an object, substituting another object for the current object.
     * @param thisArg The object to be used as the current object.
     * @param argArray A list of arguments to be passed to the method.
     */
    call(this: OAuth, thisArg: any, ...argArray: any[]): any;

    /**
     * For a given OAuth, creates a bound OAuth that has the same body as the original OAuth.
     * The this object of the bound OAuth is associated with the specified object, and has the specified initial parameters.
     * @param thisArg An object to which the this keyword can refer inside the new OAuth.
     * @param argArray A list of arguments to be passed to the new OAuth.
     */
    bind(this: OAuth, thisArg: any, ...argArray: any[]): any;

    /** Returns a string representation of a function. */
    toString(): string;

    onreadystatechange: ((this: OAuth, ev: Event) => any) | null;

    /**
     * Returns client's state.
     */
    readonly readyState: number; 

    /**
     * Returns the response body.
     */
    readonly response: any;

    /**
     * Returns response as text.
     * 
     * Throws an "InvalidStateError" DOMException if responseType is not the empty string or "text".
     */
    readonly responseText: string;

    /**
     * Returns the response type.
     * 
     * Can be set to change the response type. Values are: the empty string (default), "arraybuffer", "blob", "document", "json", and "text".
     * 
     * When set: setting to "document" is ignored if current global object is not a Window object.
     * 
     * When set: throws an "InvalidStateError" DOMException if state is loading or done.
     * 
     * When set: throws an "InvalidAccessError" DOMException if the synchronous flag is set and current global object is a Window object.
     */
    responseType: OAuthResponseType;

    readonly responseURL: string;

    readonly status: number;

    readonly statusText: string;

    /**
     * Can be set to a time in milliseconds. When set to a non-zero value will cause fetching to terminate after the given time has passed. When the time has passed, the request has not yet completed, and this's synchronous flag is unset, a timeout event will then be dispatched, or a "TimeoutError" DOMException will be thrown otherwise (for the send() method).
     * 
     * When set: throws an "InvalidAccessError" DOMException if the synchronous flag is set and current global object is a Window object.
     */
    timeout: number;

    /**
     * Returns the associated OAuthUpload object. It can be used to gather transmission information when data is transferred to a server.
     */
    readonly upload: OAuthUpload;

    /**
     * True when credentials are to be included in a cross-origin request. False when they are to be excluded in a cross-origin request and when cookies are to be ignored in its response. Initially false.
     *
     * When set: throws an "InvalidStateError" DOMException if state is not unsent or opened, or if the send() flag is set.
     */
    withCredentials: boolean;

    /**
     * Cancels any network activity.
     */
    abort(): void;
    getAllResponseHeaders(): string;
    getResponseHeader(name: string): string | null;

    /**
     * Sets the request method, request URL, and synchronous flag.
     *
     * Throws a "SyntaxError" DOMException if either method is not a valid method or url cannot be parsed.
     *
     * Throws a "SecurityError" DOMException if method is a case-insensitive match for `CONNECT`, `TRACE`, or `TRACK`.
     *
     * Throws an "InvalidAccessError" DOMException if async is false, current global object is a Window object, and the timeout attribute is not zero or the responseType attribute is not the empty string.
     */
    open(method: string, url: string | URL): void;
    open(method: string, url: string | URL, async: boolean, username?: string | null, password?: string | null): void;

    /**
     * Acts as if the `Content-Type` header value for a response is mime. (It does not change the header.)
     *
     * Throws an "InvalidStateError" DOMException if state is loading or done.
     */
    overrideMimeType(mime: string): void;

    /**
     * Initiates the request. The body argument provides the request body, if any, and is ignored if the request method is GET or HEAD.
     *
     * Throws an "InvalidStateError" DOMException if either state is not opened or the send() flag is set.
     */
    send(body?: Document | OAuthBodyInit | null): void;

    /**
     * Combines a header in author request headers.
     *
     * Throws an "InvalidStateError" DOMException if either state is not opened or the send() flag is set.
     *
     * Throws a "SyntaxError" DOMException if name is not a header name or if value is not a header value.
     */
    setRequestHeader(name: string, value: string): void;

    readonly UNSENT: 0;
    readonly OPENED: 1;
    readonly HEADERS_RECEIVED: 2;
    readonly LOADING: 3;
    readonly DONE: 4;
    addEventListener<K extends keyof OAuthEventMap>(type: K, listener: (this: OAuth, ev: OAuthEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
    addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
    removeEventListener<K extends keyof OAuthEventMap>(type: K, listener: (this: OAuth, ev: OAuthEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
    removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;
    prototype: any;
    readonly lenght: number;

    // Non-standard extensions
    arguments: any;
    caller: OAuth;
    task: OAuth;
}

interface OAuthUpload extends OAuthEventTarget {
    addEventListener<K extends keyof OAuthEventTargetEventMap>(type: K, listener: (this: OAuthUpload, ev: OAuthEventTargetEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
    addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
    removeEventListener<K extends keyof XMLHttpRequestEventTargetEventMap>(type: K, listener: (this: OAuthUpload, ev: OAuthEventTargetEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
    removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;
}

interface OAuthConstructor {
    /**
     * Creates a new OAuthes
     * @param args A list of arguments the oauth accepts.
     */
    new (...args: string[]): OAuth;
    (...args: string[]): OAuth;
    new (): OAuthConstructor;
    (): OAuthConstructor;
    new (type: OAuth, user: string): OAuth;
    (type: OAuth, user: string): OAuth;
    readonly prototype: OAuth;
}

declare var OAuth: OAuthConstructor;

interface OAuthEventMap extends OAuthEventTargetEventMap {
    "readystatechange": Event;
}

interface OAuthEventTargetEventMap {
    "abort": ProgressEvent<OAuthEventTarget>;
    "error": ProgressEvent<OAuthEventTarget>;
    "load": ProgressEvent<OAuthEventTarget>;
    "loadend": ProgressEvent<OAuthEventTarget>;
    "loadstart": ProgressEvent<OAuthEventTarget>;
    "progress": ProgressEvent<OAuthEventTarget>;
    "timeout": ProgressEvent<OAuthEventTarget>;
}

interface OAuthEventTarget extends EventTarget {
    onabort: ((this: OAuth, ev: ProgressEvent) => any) | null;
    onerror: ((this: OAuth, ev: ProgressEvent) => any) | null;
    onload: ((this: OAuth, ev: ProgressEvent) => any) | null;
    onloadend: ((this: OAuth, ev: ProgressEvent) => any) | null;
    onloadstart: ((this: OAuth, ev: ProgressEvent) => any) | null;
    onprogress: ((this: OAuth, ev: ProgressEvent) => any) | null;
    ontimeout: ((this: OAuth, ev: ProgressEvent) => any) | null;
    addEventListener<K extends keyof OAuthEventTargetEventMap>(type: K, listener: (this: OAuthEventTarget, ev: OAuthEventTargetEventMap[K]) => any, options?: boolean | AddEventListenerOptions): void;
    addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
    removeEventListener<K extends keyof OAuthEventTargetEventMap>(type: K, listener: (this: OAuthEventTarget, ev: OAuthEventTargetEventMap[K]) => any, options?: boolean | EventListenerOptions): void;
    removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;
}

interface OAuthEventTargetEventMap {
    "abort": ProgressEvent<OAuthEventTarget>;
    "error": ProgressEvent<OAuthEventTarget>;
    "load": ProgressEvent<OAuthEventTarget>;
    "loadend": ProgressEvent<OAuthEventTarget>;
    "loadstart": ProgressEvent<OAuthEventTarget>;
    "progress": ProgressEvent<OAuthEventTarget>;
    "timeout": ProgressEvent<OAuthEventTarget>;
}

declare var OAuthEventTarget: {
    prototype: OAuthEventTarget;
    new(): OAuthEventTarget;
}
interface BufferSetterConstructor {
    /**
     * A method that determines if a constructor object recognizes an object as one of the
     * constructor’s instances. Called by the semantics of the instanceof operator.
     */
    readonly hasInstance: unique symbol;

        /**
     * A Boolean value that if true indicates that an object should flatten to its array elements
     * by Array.prototype.concat.
     */
        readonly isConcatSpreadable: unique symbol;

        /**
         * A regular expression method that matches the regular expression against a string. Called
         * by the String.prototype.match method.
         */
        readonly match: unique symbol;
    
        /**
         * A regular expression method that replaces matched substrings of a string. Called by the
         * String.prototype.replace method.
         */
        readonly replace: unique symbol;
    
        /**
         * A regular expression method that returns the index within a string that matches the
         * regular expression. Called by the String.prototype.search method.
         */
        readonly search: unique symbol;
    
        /**
         * A function valued property that is the constructor function that is used to create
         * derived objects.
         */
        readonly species: unique symbol;
    
        /**
         * A regular expression method that splits a string at the indices that match the regular
         * expression. Called by the String.prototype.split method.
         */
        readonly split: unique symbol;
    
        /**
         * A method that converts an object to a corresponding primitive value.
         * Called by the ToPrimitive abstract operation.
         */
        readonly toPrimitive: unique symbol;
    
        /**
         * A String value that is used in the creation of the default string description of an object.
         * Called by the built-in method Object.prototype.toString.
         */
        readonly toStringTag: unique symbol;
    
        /**
         * An Object whose truthy properties are properties that are excluded from the 'with'
         * environment bindings of the associated objects.
         */
        readonly unscopables: unique symbol;
}
interface BufferSetter {
    [BufferSetter.toPrimitive](hint: string): symbol;

    readonly [BufferSetter.toStringTag]: string;
}

declare var BufferSetter: BufferSetterConstructor

interface BufferElementTagNameMap {
    "abbr": BufferElement;
    "address": BufferElement;
    "article": BufferElement;
    "aside": BufferElement;
    "b": BufferElement;
    "bdi": BufferElement;
    "bdo": BufferElement;
    "cite": BufferElement;
    "code": BufferElement;
    "dd": BufferElement;
    "dfn": BufferElement;
    "dt": BufferElement;
    "em": BufferElement;
    "figcaption": BufferElement;
    "figure": BufferElement;
    "footer": BufferElement;
    "header": BufferElement;
    "hgroup": BufferElement;
    "i": BufferElement;
    "kbd": BufferElement;
    "main": BufferElement;
    "mark": BufferElement;
    "nav": BufferElement;
    "noscript": BufferElement;
    "rp": BufferElement;
    "rt": BufferElement;
    "ruby": BufferElement;
    "s": BufferElement;
    "samp": BufferElement;
    "search": BufferElement;
    "section": BufferElement;
    "small": BufferElement;
    "strong": BufferElement;
    "sub": BufferElement;
    "summary": BufferElement;
    "sup": BufferElement;
    "u": BufferElement;
    "wbr": BufferElement;
}

interface Buffer<T, N extends Number> {
    readonly [BufferSetter.toStringTag]: "Buffer";

    readonly [BufferSetter.unscopables]: {
        [K in keyof any[]]?: boolean;
    }

    addItem(name: string, tags: {}, element: any[] | any ): Buffer<string, 0>;
    removeItem(name: string, tags: {}, element: any[] | any ): Buffer<string, -1>;

    /**
     * Sets or gets the URL for the current document.
     */
    readonly URL: string;
    /**
     * Sets or gets the color of all active links in the document.
     */
    alinkColor: string;
    /**
     * Retrieves a collection of all applet objects in the document.
     */
    readonly applets: BufferElement;
    /**
     * Deprecated. Sets or retrieves a value that indicates the background color behind the object.
     */
    bgColor: string;
    /**
     * Specifies the beginning and end of the document body.
     */
    body: BufferElement;
    /**
     * Returns document's encoding.
     */
    readonly characterSet: string;
    /**
     * Gets or sets the character set used to encode the object.
     */
    readonly charset: string;
    /**
     * Gets a value that indicates whether standards-compliant mode is switched on for the object.
     */
    readonly compatMode: string;
    /**
     * Returns document's content type.
     */
    readonly contentType: string;
    /**
     * Returns the HTTP cookies that apply to the Document. If there are no cookies or cookies can't be applied to this resource, the empty string will be returned.
     *
     * Can be set, to add a new cookie to the element's set of HTTP cookies.
     *
     * If the contents are sandboxed into a unique origin (e.g. in an iframe with the sandbox attribute), a "SecurityError" DOMException will be thrown on getting and setting.
     */
    cookie: string;
    /**
     * Returns the script element, or the SVG script element, that is currently executing, as long as the element represents a classic script. In the case of reentrant script execution, returns the one that most recently started executing amongst those that have not yet finished executing.
     *
     * Returns null if the Document is not currently executing a script or SVG script element (e.g., because the running script is an event handler, or a timeout), or if the currently executing script or SVG script element represents a module script.
     */
    readonly currentScript: HTMLOrSVGScriptElement | null;
    /**
     * Returns the Window object of the active document.
     */
    readonly defaultView: (WindowProxy & typeof globalThis) | null;
    /**
     * Sets or gets a value that indicates whether the document can be edited.
     */
    designMode: string;
    /**
     * Sets or retrieves a value that indicates the reading order of the object.
     */
    dir: string;
    /**
     * Gets an object representing the document type declaration associated with the current document.
     */
    readonly doctype: DocumentType | null;
    /**
     * Gets a reference to the root node of the document.
     */
    readonly documentElement: BufferElement;
    /**
     * Returns document's URL.
     */
    readonly documentURI: string;
    /**
     * Sets or gets the security domain of the document.
     */
    domain: string;
    /**
     * Retrieves a collection of all embed objects in the document.
     */
    readonly embeds: HTMLCollectionOf<HTMLEmbedElement>;
    /**
     * Sets or gets the foreground (text) color of the document.
     */
    fgColor: string;
    /**
     * Retrieves a collection, in source order, of all form objects in the document.
     */
    readonly forms: HTMLCollectionOf<HTMLFormElement>;
    readonly fullscreen: boolean;
    /**
     * Returns true if document has the ability to display elements fullscreen and fullscreen is supported, or false otherwise.
     */
    readonly fullscreenEnabled: boolean;
    /**
     * Returns the head element.
     */
    readonly head: HTMLHeadElement;
    readonly hidden: boolean;
    /**
     * Retrieves a collection, in source order, of img objects in the document.
     */
    readonly images: HTMLCollectionOf<HTMLImageElement>;
    /**
     * Gets the implementation object of the current document.
     */
    readonly implementation: DOMImplementation;
    /**
     * Returns the character encoding used to create the webpage that is loaded into the document object.
     */
    readonly inputEncoding: string;
    /**
     * Gets the date that the page was last modified, if the page supplies one.
     */
    readonly lastModified: string;
    /**
     * Sets or gets the color of the document links.
     */
    linkColor: string;
    /**
     * Contains information about the current URL.
     */
    get location(): Location;
    set location(href: string | Location);
    onfullscreenchange: ((this: Buffer, ev: Event) => any) | null;
    onfullscreenerror: ((this: Buffer, ev: Event) => any) | null;
    onpointerlockchange: ((this: Buffer, ev: Event) => any) | null;
    onpointerlockerror: ((this: Buffer, ev: Event) => any) | null;
    /**
     * Fires when the state of the object has changed.
     * @param ev The event
     */
    onreadystatechange: ((this: Buffer, ev: Event) => any) | null;
    onvisibilitychange: ((this: Buffer, ev: Event) => any) | null;
    readonly ownerDocument: null;
    readonly pictureInPictureEnabled: boolean;
    /**
     * Retrieves a value that indicates the current state of the object.
     */
    readonly readyState: DocumentReadyState;
    /**
     * Gets the URL of the location that referred the user to the current page.
     */
    readonly referrer: string;
    readonly rootElement: SVGSVGElement | null;
    /**
     * Retrieves a collection of all script objects in the document.
     */
    readonly scripts: HTMLCollectionOf<HTMLScriptElement>;
    readonly scrollingElement: Element | null;
    readonly timeline: DocumentTimeline;
    /**
     * Contains the title of the document.
     */
    title: string;
    readonly visibilityState: DocumentVisibilityState;
    /**
     * Sets or gets the color of the links that the user has visited.
     */
    vlinkColor: string;
    /**
     * Moves node from another document and returns it.
     *
     * If node is a document, throws a "NotSupportedError" DOMException or, if node is a shadow root, throws a "HierarchyRequestError" DOMException.
     */
    adoptNode<T extends Node>(node: T): T;
    captureEvents(): void;
    caretRangeFromPoint(x: number, y: number): Range | null;
    clear(): void;
    /**
     * Closes an output stream and forces the sent data to display.
     */
    close(): void;
    /**
     * Creates an attribute object with a specified name.
     * @param name String that sets the attribute object's name.
     */
    createAttribute(localName: string): Attr;
    
    createAttributeNS(namespace: string | null, qualifiedName: string): Attr;
    /**
     * Returns a CDATASection node whose data is data.
    createCDATASection(data: string): CDATASection;
    /**
     * Creates an instance of the element for the specified tag.
     * @param tagName The name of an element.
     */
    createElement<K extends keyof BufferElementTagNameMap>(tagName: K, options?: ElementCreationOptions): BufferElementTagNameMap[K];
    /**
     * Returns an element with namespace namespace. Its namespace prefix will be everything before ":" (U+003E) in qualifiedName or null. Its local name will be everything after ":" (U+003E) in qualifiedName or qualifiedName.
     *
     * If localName does not match the Name production an "InvalidCharacterError" DOMException will be thrown.
     *
     * If one of the following conditions is true a "NamespaceError" DOMException will be thrown:
     *
     * localName does not match the QName production.
     * Namespace prefix is not null and namespace is the empty string.
     * Namespace prefix is "xml" and namespace is not the XML namespace.
     * qualifiedName or namespace prefix is "xmlns" and namespace is not the XMLNS namespace.
     * namespace is the XMLNS namespace and neither qualifiedName nor namespace prefix is "xmlns".
     *
     * When supplied, options's is can be used to create a customized built-in element.
     */
    createElementNS(namespaceURI: "http://www.w3.org/1999/xhtml", qualifiedName: string): HTMLElement;
    createElementNS<K extends keyof SVGElementTagNameMap>(namespaceURI: "http://www.w3.org/2000/svg", qualifiedName: K): SVGElementTagNameMap[K];
    createElementNS(namespaceURI: "http://www.w3.org/2000/svg", qualifiedName: string): SVGElement;
    createElementNS<K extends keyof MathMLElementTagNameMap>(namespaceURI: "http://www.w3.org/1998/Math/MathML", qualifiedName: K): MathMLElementTagNameMap[K];
    createElementNS(namespaceURI: "http://www.w3.org/1998/Math/MathML", qualifiedName: string): MathMLElement;
    createElementNS(namespaceURI: string | null, qualifiedName: string, options?: ElementCreationOptions): Element;
    createElementNS(namespace: string | null, qualifiedName: string, options?: string | ElementCreationOptions): Element;
    
    createEvent(eventInterface: "AnimationEvent"): AnimationEvent;
    createEvent(eventInterface: "AnimationPlaybackEvent"): AnimationPlaybackEvent;
    createEvent(eventInterface: "AudioProcessingEvent"): AudioProcessingEvent;
    createEvent(eventInterface: "BeforeUnloadEvent"): BeforeUnloadEvent;
    createEvent(eventInterface: "BlobEvent"): BlobEvent;
    createEvent(eventInterface: "ClipboardEvent"): ClipboardEvent;
    createEvent(eventInterface: "CloseEvent"): CloseEvent;
    createEvent(eventInterface: "CompositionEvent"): CompositionEvent;
    createEvent(eventInterface: "CustomEvent"): CustomEvent;
    createEvent(eventInterface: "DeviceMotionEvent"): DeviceMotionEvent;
    createEvent(eventInterface: "DeviceOrientationEvent"): DeviceOrientationEvent;
    createEvent(eventInterface: "DragEvent"): DragEvent;
    createEvent(eventInterface: "ErrorEvent"): ErrorEvent;
    createEvent(eventInterface: "Event"): Event;
    createEvent(eventInterface: "Events"): Event;
    createEvent(eventInterface: "FocusEvent"): FocusEvent;
    createEvent(eventInterface: "FontFaceSetLoadEvent"): FontFaceSetLoadEvent;
    createEvent(eventInterface: "FormDataEvent"): FormDataEvent;
    createEvent(eventInterface: "GamepadEvent"): GamepadEvent;
    createEvent(eventInterface: "HashChangeEvent"): HashChangeEvent;
    createEvent(eventInterface: "IDBVersionChangeEvent"): IDBVersionChangeEvent;
    createEvent(eventInterface: "InputEvent"): InputEvent;
    createEvent(eventInterface: "KeyboardEvent"): KeyboardEvent;
    createEvent(eventInterface: "MIDIConnectionEvent"): MIDIConnectionEvent;
    createEvent(eventInterface: "MIDIMessageEvent"): MIDIMessageEvent;
    createEvent(eventInterface: "MediaEncryptedEvent"): MediaEncryptedEvent;
    createEvent(eventInterface: "MediaKeyMessageEvent"): MediaKeyMessageEvent;
    createEvent(eventInterface: "MediaQueryListEvent"): MediaQueryListEvent;
    createEvent(eventInterface: "MediaStreamTrackEvent"): MediaStreamTrackEvent;
    createEvent(eventInterface: "MessageEvent"): MessageEvent;
    createEvent(eventInterface: "MouseEvent"): MouseEvent;
    createEvent(eventInterface: "MouseEvents"): MouseEvent;
    createEvent(eventInterface: "MutationEvent"): MutationEvent;
    createEvent(eventInterface: "MutationEvents"): MutationEvent;
    createEvent(eventInterface: "OfflineAudioCompletionEvent"): OfflineAudioCompletionEvent;
    createEvent(eventInterface: "PageTransitionEvent"): PageTransitionEvent;
    createEvent(eventInterface: "PaymentMethodChangeEvent"): PaymentMethodChangeEvent;
    createEvent(eventInterface: "PaymentRequestUpdateEvent"): PaymentRequestUpdateEvent;
    createEvent(eventInterface: "PictureInPictureEvent"): PictureInPictureEvent;
    createEvent(eventInterface: "PointerEvent"): PointerEvent;
    createEvent(eventInterface: "PopStateEvent"): PopStateEvent;
    createEvent(eventInterface: "ProgressEvent"): ProgressEvent;
    createEvent(eventInterface: "PromiseRejectionEvent"): PromiseRejectionEvent;
    createEvent(eventInterface: "RTCDTMFToneChangeEvent"): RTCDTMFToneChangeEvent;
    createEvent(eventInterface: "RTCDataChannelEvent"): RTCDataChannelEvent;
    createEvent(eventInterface: "RTCErrorEvent"): RTCErrorEvent;
    createEvent(eventInterface: "RTCPeerConnectionIceErrorEvent"): RTCPeerConnectionIceErrorEvent;
    createEvent(eventInterface: "RTCPeerConnectionIceEvent"): RTCPeerConnectionIceEvent;
    createEvent(eventInterface: "RTCTrackEvent"): RTCTrackEvent;
    createEvent(eventInterface: "SecurityPolicyViolationEvent"): SecurityPolicyViolationEvent;
    createEvent(eventInterface: "SpeechSynthesisErrorEvent"): SpeechSynthesisErrorEvent;
    createEvent(eventInterface: "SpeechSynthesisEvent"): SpeechSynthesisEvent;
    createEvent(eventInterface: "StorageEvent"): StorageEvent;
    createEvent(eventInterface: "SubmitEvent"): SubmitEvent;
    createEvent(eventInterface: "ToggleEvent"): ToggleEvent;
    createEvent(eventInterface: "TouchEvent"): TouchEvent;
    createEvent(eventInterface: "TrackEvent"): TrackEvent;
    createEvent(eventInterface: "TransitionEvent"): TransitionEvent;
    createEvent(eventInterface: "UIEvent"): UIEvent;
    createEvent(eventInterface: "UIEvents"): UIEvent;
    createEvent(eventInterface: "WebGLContextEvent"): WebGLContextEvent;
    createEvent(eventInterface: "WheelEvent"): WheelEvent;
    createEvent(eventInterface: string): Event;
    /**
     * Creates a NodeIterator object that you can use to traverse filtered lists of nodes or elements in a document.
     * @param root The root element or node to start traversing on.
     * @param whatToShow The type of nodes or elements to appear in the node list
     * @param filter A custom NodeFilter function to use. For more information, see filter. Use null for no filter.
     */
    createNodeIterator(root: Node, whatToShow?: number, filter?: NodeFilter | null): NodeIterator;
    /**
     * Returns a ProcessingInstruction node whose target is target and data is data. If target does not match the Name production an "InvalidCharacterError" DOMException will be thrown. If data contains "?>" an "InvalidCharacterError" DOMException will be thrown.
     */
    createProcessingInstruction(target: string, data: string): ProcessingInstruction;
    /**
     * Returns an empty range object that has both of its boundary points positioned at the beginning of the document.
     */
    createRange(): Range;
    /**
     * Creates a text string from the specified value.
     * @param data String that specifies the nodeValue property of the text node.
     */
    createTextNode(data: string): Text;
    /**
     * Creates a TreeWalker object that you can use to traverse filtered lists of nodes or elements in a document.
     * @param root The root element or node to start traversing on.
     * @param whatToShow The type of nodes or elements to appear in the node list. For more information, see whatToShow.
     * @param filter A custom NodeFilter function to use.
     */
    createTreeWalker(root: Node, whatToShow?: number, filter?: NodeFilter | null): TreeWalker;
    /**
     * Executes a command on the current document, current selection, or the given range.
     * @param commandId String that specifies the command to execute. This command can be any of the command identifiers that can be executed in script.
     * @param showUI Display the user interface, defaults to false.
     * @param value Value to assign.
     */
    exexCommand(commandId: string, showUI?: boolean, value?: string): boolean;

    exitFullscreen(): Promise<void>;

    exitBufferInBuffer(): Promise<void>;

    exitPointerLock(): void;

    getItemById(itemId: string): BufferElement | null;
    getItemsByName(itemName: string): NodeListOf<BufferElement>;
    /**
     * Returns an object representing the current selection of the document that is loaded into the object displaying a webpage.
     *
     * [MDN Reference](https://developer.mozilla.org/docs/Web/API/Document/getSelection)
     */
    getSelection(): Selection | null;
    /**
     * Gets a value indicating whether the object currently has focus.
     */
    hasFocus(): boolean;
    hasStorageAccess(): Promise<boolean>;
    /**
     * Returns a copy of node. If deep is true, the copy also includes the node's descendants.
     *
     * If node is a document or a shadow root, throws a "NotSupportedError" DOMException.
     */
    importNode<T extends Node>(node: T, deep?: boolean): T;
    /**
     * Opens a new window and loads a document specified by a given URL. Also, opens a new window that uses the url parameter and the name parameter to collect the output of the write method and the writeln method.
     * @param url Specifies a MIME type for the document.
     * @param name Specifies the name of the window. This name is used as the value for the TARGET attribute on a form or an anchor element.
     * @param features Contains a list of items separated by commas. Each item consists of an option and a value, separated by an equals sign (for example, "fullscreen=yes, toolbar=yes"). The following values are supported.
     * @param replace Specifies whether the existing entry for the document is replaced in the history list.
     */
    open(unused1?: string, unused2?: string): Buffer;
    open(url: string | URL, name: string, features: string): WindowProxy | null;
    /**
     * Returns a Boolean value that indicates whether a specified command can be successfully executed using execCommand, given the current state of the document.
     * @param commandId Specifies a command identifier.
     */
    queryCommandEnabled(commandId: string): boolean;
    /**
     * Returns a Boolean value that indicates whether the specified command is in the indeterminate state.
     * @param commandId String that specifies a command identifier.
     */
    queryCommandIndeterm(commandId: string): boolean;
    /**
     * Returns a Boolean value that indicates the current state of the command.
     * @param commandId String that specifies a command identifier.
     */
    queryCommandState(commandId: string): boolean;
    /**
     * Returns a Boolean value that indicates whether the current command is supported on the current range.
     * @param commandId Specifies a command identifier.
     */
    queryCommandSupported(commandId: string): boolean;
    /**
     * Returns the current value of the document, range, or current selection for the given command.
     * @param commandId String that specifies a command identifier.
     */
    queryCommandValue(commandId: string): string;
    releaseEvents(): void;
    requestStorageAccess(): Promise<void>;
    /**
     * Writes one or more HTML expressions to a document in the specified window.
     * @param content Specifies the text and HTML tags to write.
     */
    write(...text: string[]): void;
    /**
     * Writes one or more HTML expressions, followed by a carriage return, to a document in the specified window.
     * @param content The text and HTML tags to write.
     */
    writeln(...text: string[]): void;
    addBufferListener<K extends keyof BufferEventMap>(type: K, listener: (this: Buffer, ev: BufferEventMap[K]) => any, options?: boolean | AddBufferListenerOptions): void;
    addBufferListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddBufferListenerOptions): void;
    removeBufferListener<K extends keyof BufferEventMap>(type: K, listener: (this: Buffer, ev: BufferEventMap[K]) => any, options?: boolean | BufferListenerOptions): void;
    removeBufferListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | BufferListenerOptions): void;
}

interface AddBufferListenerOptions extends BufferListenerOptions {
    once?: boolean;
    passive?: boolean;
    signal?: AbortSignal;
}

interface BufferListenerOptions {
    capture?: boolean;
}

interface BufferEventMap2 extends ElementEventMap, GlobalEventHandlersEventMap {
}

/**
 * Any Buffer element. Some elements directly implement this interface, while others implement it via an interface that inherits it.
 */
interface BufferElement {
    acessKey: string;
    readonly accessKeyLabel: string;
    autocapitalize: string;
    dir: string;
    draggable: boolean;
    hidden: boolean;
    inert: boolean;
    innerText: string;
    lang: string;
    readonly offsetHeight: number;
    readonly offsetLeft: number;
    readonly offsetParent: Element | null;
    readonly offsetTop: number;
    readonly offsetWidth: number;
    outerText: string;
    popover: string | null;
    spellcheck: boolean;
    title: string;
    translate: boolean;
    attachInternals(): ElementInternals;
    click(): void;
    hidePopover(): void;
    showPopover(): void;
    togglePopover(force?: boolean): void;
    addEventListener<K extends keyof BufferEventMap2>(type: K, listener: (this: BufferElement, ev: BufferEventMap2[K]) => any, options?: boolean | AddBufferListenerOptions): void;
    addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;
    removeEventListener<K extends keyof BufferEventMap2>(type: K, listener: (this: BufferElement, ev: BufferEventMap2[K]) => any, options?: boolean | BufferListenerOptions): void;
    removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | BufferListenerOptions): void;
}

declare var BufferElement: {
    prototype: BufferElement;
    new(): BufferElement;
}

interface BufferEventMap extends GlobalEventHandlersEventMap {
    "DOMContentLoaded": Event;
    "fullscreenchange": Event;
    "fullscreenerror": Event;
    "pointerlockchange": Event;
    "pointerlockerror": Event;
    "readystatechange": Event;
    "visibilitychange": Event;
}

interface ReadonlyBuffer<T, N extends Number> {
    readonly [BufferSetter.unscopables]: {
        [K in keyof readonly any[]]?: boolean;
    }

    readonly buffer: Buffer<T, N>;
}

type OAuthBodyInit = Blob | BufferSource | FormData | URLSearchParams | string;
type OAuthResponseType = "" | "arraybuffer" | "blob" | "document" | "json" | "text";
