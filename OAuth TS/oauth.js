// Your script or this script

/** 
 * NReum UA OAuth Client 
 * Do not copy this code! 
 * Copyright (C) 2023 NReum Corp. 
 */

/** 
 * Return a random value to Ray ID 
 */
function random() {
    const array = new Uint32Array(1);
    crypto.getRandomValues(array);
    const ray_id = array[0] % 100000 + 1;
    return ray_id;
}
var ray_id = random();
(function() {

    let pre = document.createElement('pre');
    pre.className = "ray";
    pre.innerHTML = `${ray_id}`;

    document.body.append(pre);
})();

function scan(type = '', time = new Date().setTime()) {
    if (type == 'ddos') {
        if (time > 0) {
            return 0
        } else {
            let rel = document.createElement('meta');
            rel.httpEquiv = 'refresh';
            rel.content = 5;

            document.head.append(rel);
            return 1;
        }
    }
    return new Date().getTime();
}

function innerScan() {
    if (ray_id > 0) {
        return true;
    }
    return false;
}

let scan_inner = innerScan();
let ret = scan('ddos', new Date().setTime(10000));
if (scan_inner == true) {
    scan('ddos', new Date().setTime(10000));
    let go = document.createElement('meta');
    go.httpEquiv = 'refresh';
    go.content = '0; url=your_web_site';
    if (ret == 1) {
        document.head.append(go);
    }
    document.head.append(go);
} else if (scan_inner == false) {
    let error = document.createElement('pre')
    error.className = 'error'
    error.innerHTML = '<strong>Error of scanning. Please reload this page</strong>'

    document.body.append(error);
}
let OAuth_Protocol = generateOAuth("OAuth2P", 867487867454584956862452957386288574, 1, false);
/**
 * This function generate `OAuth2` protocol.
 * @param {*} type this parameter set protocol type
 * @param {*} sha256 this parameter set UUID to protocol
 * @param {*} id this parameter set ID in protocol
 * @param {*} cert this parameter set generation protocol certificate
 */
function generateOAuth(type = "", sha256 = 0, id = 0, cert = false) {
    if (type == "OAuth2P" || sha256 != 0 || id != 0) {
        if (cert == true) {
            generateOAuthCertificate(".crt", "OAuth" + sha256);
            let crt = generateOAuthCertificate(".crt", "OAuth" + sha256);
            let OAuth2 = type + ":" + sha256 + id + "Proto" + crt;
            return OAuth2;
        } else {
            let OAuth = type + ":" + sha256 + id + "Proto";
            return OAuth;
        }
    } else if (type == "OAuth2D" || sha256 != 0 || id != 0) {
        if (cert == true) {
            generateOAuthCertificate(".dec", "OAuth" + sha256);
            let crt = generateOAuthCertificate(".crt", "OAuth" + sha256);
            let OAuth2 = type + ":" + sha256 + id + "Proto" + crt;
            return OAuth2;
        } else {
            let OAuth = type + ":" + sha256 + id + "Proto";
            return OAuth;
        }
    }
    return "";
}
/**
 * This function generate `certificate` to protocol
 * @param {*} type set certificate type
 * @param {*} owner set certificate owner
 */
function generateOAuthCertificate(type = "", owner = "") {
    if (type == ".crt" || owner != "") {
        let cert = document.createElement('script')
        cert.type = "ssl/crt";
        cert.async = true;
        cert.defer = "OAuth2 Proto"
        cert.innerHTML = `
        var certificate = '74566959406867448759867877646786476984764674674986457647637540385496847209540673985834095HUMt8498yttu84yheog8y87yg87niy87gy4g874yng87gtg8ieyfo8it7END${owner};
        function export() {
            return certificate;
        }
        console.log(export());
        `
        document.body.append(cert)
        return `74566959406867448759867877646786476984764674674986457647637540385496847209540673985834095HUMt8498yttu84yheog8y87yg87niy87gy4g874yng87gtg8ieyfo8it7END${owner}`;
    } else if (type == ".dec" || owner != "") {
        let cert = document.createElement('script')
        cert.type = "ssl/crt";
        cert.async = true;
        cert.defer = "OAuth2 Proto"
        cert.innerHTML = `
        var certificate = '10101010101101010011110101001010101010101101010101011011101010010101010101010101010101010101010101010100110000011010101010101001100010110010101010END${owner};
        function export() {
            return certificate;
        }
        console.log(export());
        `
        document.body.append(cert)
        return `10101010101101010011110101001010101010101101010101011011101010010101010101010101010101010101010101010100110000011010101010101001100010110010101010END${owner}`;
    }
}

/* ----------------------------------------OAuth Protocol 1.1---------------------------------------- */

var OAUTH_PROTOCOL_OK = 200;
var OAUTH_PROTOCOL_ERROR = 404;
var OAUTH_PROTOCOL_UNDEFINED = 401;
var OAUTH_PROTOCOL_NOT_GEN = 402;
var OAUTH_PROTOCOL_LOAD = 201;
var OAUTH_PROTOCOL_SUCCESS = 202;
var OAUTH_PROTOCOL_SCAN = 301;
var RAYID_ERROR = 403;
var RAYID_SUCCESS = 205;

function protocolFilter() {
    if (OAuth_Protocol != "") {
        return OAUTH_PROTOCOL_OK;
    } else {
        return OAUTH_PROTOCOL_ERROR;
    }
}

// OAuth Request
import "./lib.oauth"
let oauth = new OAuth();
oauth.open('GET', 'http://nreum.ua/api.php', true);
oauth.onreadystatechange = function() {
    if (oauth.readyState === 4) {
        if (oauth.status === 200) {
            console.log(xhr.responseText);
        } else {
            console.log('Request failed with status ' + oauth.status);
        }
    }
};
oauth.send();